/* Vous devez rédiger ce que vous avez compris du sujet liste de courses en 4 à 5 lignes et rappeler le travail que nous avons déjà effectué sur ce sujet */

/* le merge request qui sera retenu (un seul) sera celui qui est le plus clair et le mieux détaillé et sans faute d'orthographe */

----------------------------------------------------------------------------------------------------------------------------------------

Cas n°1 

Nom : Accéder à mes liste de course
Acteur(s) : Utilisateur (client)
Description : La consultation à mes listes de course doit être possible pour un utilisateur.
Auteur : Simon Cavillon
Date(s) : 08/11/2020

Pré-conditions : L’utilisateur doit être authentifié à l'application (Cas d’utilisation « S’authentifier »)
Démarrage : L’utilisateur a demandé la page « Consultation mes listes de courses »

DESCRIPTION

Le scénario nominal :

1. Le système affiche une page contenant les liste de course de l'utilisateur ainsi qu'un bouton créer une liste de course (Cas d’utilisation « Créer une liste de course »). 
2. L’utilisateur sélectionne une des liste de course.
3. Le système recherche la liste de course parmis les autres listes de course.
4. Le système affiche une page contenant les informations de la liste de course avec les produits contenus (Quantité et prix) ainsi que le panier avec le bouton communiquer (Cas d’utilisation « Communiquer une liste de course »).
5. L’utilisateur peut ensuite appuyer sur le bouton "communiquer".
6. Le système affiche 2 options sous formes de bouton pour que l'utilisateur choisi dans quelle forme la liste de course doit être communiquer. 
7. L’utilisateur peut ensuite sélectionner l'option qu'il désire.

Les scénarios alternatifs

2.a L’utilisateur décide de créer une nouvelle liste de course.
2.b L’utilisateur décide de quitter la consultation de ses listes de course. 
5.a L’utilisateur décide de quitter la consultation de sa liste de course sélectionnée.
5.b L’utilisateur décide de quitter la consultation du catalogue.
7.a L’utilisateur décide de choisir l'option "Smartphone".
7.b L’utilisateur décide de choisir l'option "Imprimante".
7.c L’utilisateur décide d'annuler la communication de sa liste de course.

Fin : Scénario nominal : aux étapes 2, 5 ou 7, sur décision de l’utilisateur

Post-conditions : Aucun
